﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private List<Level> _levels;
    [SerializeField] private GameObject _levelContainer;
    [SerializeField] private UiGameScreen _gamescreen = null;
    [SerializeField] private AdsManager _adsManager = null;

    private GameObject _levelObject = null;

    public void Start()
    {
        GameAnalyticsSDK.GameAnalytics.Initialize();
        _adsManager.Initialize();
    }
    public void StartGame()
    {
        int index = Random.Range(0, _levels.Count);  // Unity с интами последнее значение в рандоме не включает!!!!!!!!!!! с флотиками включает
        InstaniateLevel(index);
    }

    private void InstaniateLevel(int index)
    {
        if (_levelObject != null)
        {
            Destroy(_levelObject);
            _levelObject = null; // мы хотим удалить в этом же кадре, на всякий случай. Если не делать этого, удалит в след кадре
        }

        if (index >= _levels.Count)
        {
            return;
        }
        _levelObject = Instantiate(_levels[index].gameObject, _levelContainer.transform); // Instantiate - метод юнити для создания объектов  // _levelContainer.transform - перемещаем новый объект сюда
        Level level = _levelObject.GetComponent<Level>();
        level.Initialized();
        _gamescreen.Initialize(level);
        level.OnLevelCompleted += OnLevelComplete; // в левеле ищем компонент - дубликат, и пихаем в него метод - онлевел комплит.
    }
    public void OnLevelComplete()
    {
        CoroutineHelper.DelayCall(this, WinDebugLog, 2f ); //this - monobeh obj
        GameAnalyticsSDK.GameAnalytics.NewDesignEvent("Level Compl");
        _adsManager.ShowInterstitial(); //вращайте барабан
    }
    private void WinDebugLog()
    {
        Debug.Log("Win");
    }
}

