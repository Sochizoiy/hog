﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiGameScreen : MonoBehaviour
{
    [SerializeField] private RectTransform _content = null;
    [SerializeField] private GameObject _itemPrefab = null;

    public void Initialize(Level level)
    {
        Dictionary<string, ItemData> _items = level.GetItemsData();
        foreach (string key in _items.Keys) //перебираем не айтемы а ключи
        {
            GameObject newItem = Instantiate(_itemPrefab, _content);
            newItem.GetComponent<UIItemController>().Initialize(_items[key].ItemSprite, _items[key].Amount);
            // newitem - геймобджект, геткомпонент - пытаемся выдернуть класс с геймобджекта (могут быть неск скриптов)
            // инициализируем, принимая спрайт и кол-во 
        }
    }
}
