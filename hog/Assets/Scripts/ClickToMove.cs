﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickToMove : MonoBehaviour
{
    [SerializeField] private float speed = 0f;
    [SerializeField] private GameObject _firstPoint = null;
    [SerializeField] private GameObject _secondPoint = null;


    private Vector3 tochka1;
    private Vector3 tochka2;
    private Vector3 target;

    void Start()
    {
        tochka1 = _firstPoint.transform.position;
        tochka2 = _secondPoint.transform.position;
        target = tochka2;
    }


    void Update()
    {
        if (transform.position != target)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, (speed * Time.deltaTime)); //не понимаю что делаю, но работает

        }
    }
    private void OnMouseUp()
    {
        if (target == tochka2)
        {
            target = tochka1;
        }
        else
        {
            target = tochka2;
        }
    }
}
