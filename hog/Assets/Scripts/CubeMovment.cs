﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovment : MonoBehaviour
{
    [SerializeField] private float _speed = 0f;
    [SerializeField] private GameObject _secondCube = null;

    private float _epsilon = 1f;

    private Vector3 _startPosition;
    private Vector3 _direction;
    private Vector3 _targetPosition;

    private void Start()
    {
        _startPosition = transform.position;
        _direction = _secondCube.transform.position - transform.position;
        _targetPosition = _secondCube.transform.position;
        _direction.Normalize();
    }
    void Update()
    {
        float distance = Vector3.Distance(transform.position, _targetPosition);
        if (distance > _epsilon)
        {
            Vector3 newPosition = transform.position + (_direction * _speed * Time.deltaTime);
            transform.position = newPosition;
        }
        else
        {
            _speed *= -1f;
            _targetPosition = _startPosition;

        }
    }
}
