﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdsManager : MonoBehaviour
{
    [SerializeField] private string _androidKey = "85460dcd";
    public void Initialize()
    {
        IronSource.Agent.init(_androidKey);
        IronSource.Agent.validateIntegration();
        IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.BOTTOM);
        IronSource.Agent.loadInterstitial();
    }
    private void OnEnable()
    {
        IronSourceEvents.onInterstitialAdReadyEvent += OnInterstitialLoaded;
        IronSourceEvents.onInterstitialAdLoadFailedEvent += OnInterstitialLoadFailed;
        IronSourceEvents.onInterstitialAdShowSucceededEvent += OnInterstitialShowSuccess;
        IronSourceEvents.onInterstitialAdClosedEvent += OnInterstitialClosed;
    }
    private void OnDisable()
    {
        IronSourceEvents.onInterstitialAdReadyEvent -= OnInterstitialLoaded;
        IronSourceEvents.onInterstitialAdLoadFailedEvent -= OnInterstitialLoadFailed;
        IronSourceEvents.onInterstitialAdShowSucceededEvent -= OnInterstitialShowSuccess;
        IronSourceEvents.onInterstitialAdClosedEvent -= OnInterstitialClosed;
    }
    private bool _isInterStitialReady = false;
    private void OnInterstitialLoaded()
    {
        _isInterStitialReady = true;
    }
    private void OnInterstitialLoadFailed(IronSourceError error)
    {
        Debug.LogError($"Interstitial load failed! Reason: {error.getDescription()}");
    }
    private void OnInterstitialShowSuccess()
    {
        _isInterStitialReady = false;
    }
    private void OnInterstitialClosed()
    {
        IronSource.Agent.loadInterstitial();
    }

    public void ShowInterstitial()
    {
        if (_isInterStitialReady)
        {
            IronSource.Agent.showInterstitial();
        }
    }
}
