﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIItemController : MonoBehaviour
{
    [SerializeField] private Text _countText = null;
    [SerializeField] private Image _image = null;

    private int _count = 0;

    public void Initialize(Sprite sprite, int count)
    {
        _image.sprite = sprite;
        _count = count;
        _countText.text = count.ToString(); // у нашего компонента Текст есть не только текст, но и шрифт и тд. Мы обращаемся только к тексту.
    }

    public void Decrease()
    {
        _count--;

        if (_count == 0)
        {
            gameObject.SetActive(false); // отключаем UIItem на котором висит скрипт
        }

        else
        {
            _countText.text = _count.ToString();
        }
    }
}
