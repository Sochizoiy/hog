﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    private List<Item> _gameItems = new List<Item>();
    private Dictionary<string, ItemData> _itemData = new Dictionary<string, ItemData>();

    public delegate void OnComplete(); // правильно - экшен, но это для примера
    public OnComplete OnLevelCompleted;



    public void Initialized()
    {
        _gameItems = new List<Item>(GetComponentsInChildren<Item>()); //хранит список наших итемов, создает список со старта игры всех наших приколов
        // _gameItems.Add(new Item());
        //_gameItems.Remove(); //так мы работаем с листом
        foreach (var i in _gameItems)
        {
            i.Initialized();
            i.OnFind += OnFindAction; //подписываемся на событие

            if (!_itemData.ContainsKey(i.name))
            {
                _itemData.Add(i.name, new ItemData(i.ItemSprite, 1));
            }
            else
            {
                _itemData[i.Name].Amount++; //amount сами писали метод, всё гуд 
            }
        }

    }
    public Dictionary<string, ItemData> GetItemsData() //без string, ItemData не работает
    {
        return _itemData;
    }
    private void Start()
    {
        Initialized();
    }
    private void OnFindAction(Item item)
    {
        _gameItems.Remove(item);
        item.OnFind -= OnFindAction;

        if (_gameItems.Count == 0)
        {
            OnLevelCompleted?.Invoke(); // пытаемся инвокнуть всё что в нем лежит
        }
    }
}
