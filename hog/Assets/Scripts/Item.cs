﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Item : MonoBehaviour
{
    [SerializeField] private string _name = string.Empty;
    [Header("Setting")]
    [SerializeField] private float _scaleMultiplier;
    [SerializeField] private float _scaleTime;
    [SerializeField] private float _disapearTime;

    private SpriteRenderer _spriteRenderer;
    public System.Action<Item> OnFind;


    public void Initialized()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }
    public string Name  // это не переменная и не метод, это свойство для работы с _name
    {
        get
        {
            return _name;
        }
        set
        {
            _name = value;
        }
    }
    private void Awake()
    {
        Initialized();
    }
    void OnMouseDown()
    {
        FindAction();
    }
    private Coroutine _coroutine = null;
    void FindAction()
    {
        _coroutine = StartCoroutine(Scaling()); //никогда не StartCoroutine("Scaling")
        // StopCoroutine(_coroutine); // вот так где надо выключим ее 
        //код ниже мы заменили на корутину

        /*Vector3 newScale = transform.localScale * _scaleMultiplier;
       transform.DOScale(newScale, _scaleTime).OnComplete(
           
       ()=>
       {
           _spriteRenderer.DOFade(0f, _disapearTime).OnComplete(DisableObject);
       });*/

    }
    public Sprite ItemSprite => _spriteRenderer.sprite; // сокращенное св-во, только с гетом 

    private void DisableObject()
    {
        gameObject.SetActive(false);
        OnFind?.Invoke(this); //ууууууу!!! ? - чтобы не было ошибок, инвок - вызывает все методы, которые содержит. В скобках = сигнатура. Все методы должны быть одной сигны.
    }
    //корутины
    private IEnumerator Scaling()
    {
        Vector3 startScale = transform.localScale;
        float currentMultiplier = 1f;
        float speed = (_scaleMultiplier - currentMultiplier) / _scaleTime;

        while (currentMultiplier < _scaleMultiplier)
        {
            Vector3 currentScale = startScale * currentMultiplier;
            //currentMultiplier += speed * Time.deltaTime;
            currentMultiplier += Mathf.Lerp(1f, _scaleMultiplier,speed*Time.deltaTime) - 1f; //пример лерпа
            transform.localScale = currentScale;
            Debug.Log(currentMultiplier);
            yield return null;
        }
        yield return new WaitForSeconds(1f);

        float currentAplha = 1f;
        float alphaSpeed = 1f / _disapearTime;

        while (currentAplha > 0f)
        {
            Color color = _spriteRenderer.color;
            color.a = currentAplha; //так получили доступ к компоненту а, тк забрали в переменную
            currentAplha -= alphaSpeed * Time.deltaTime;
            _spriteRenderer.color = color;

            yield return null;
        }
        DisableObject();
    }
}
